<?php


namespace tests\libraries\utils;

use libraries\utils\YAMLParser;

class YAMLParserTest extends \PHPUnit_Framework_TestCase
{
    public function testParse() {
        $parser = new YAMLParser();
        
        $config = $parser->parse('../../fixtures/config.yml');
        print_r($config);
    }
}
