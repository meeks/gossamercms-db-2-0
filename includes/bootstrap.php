<?php

use libraries\utils\YAMLParser;
use core\eventlisteners\EventDispatcher;
use core\system\KernelEvents;




$eventDispatcher = new EventDispatcher(null, $container->get('Logger'));
iterateComponentConfigurations($eventDispatcher);

use core\system\Kernel;

//$eventDispatcher->dispatch('test_event', 'sayHello');
$container->set('EventDispatcher', null, $eventDispatcher);

function iterateComponentConfigurations(EventDispatcher $eventDispatcher) {
    global $logger;
    $parser = new YAMLParser($logger);
    $parser->setFilePath(__SITE_PATH . '/config/bootstrap.yml');
    $bootstraps = $parser->loadConfig(); //$parser->findNodeByURI(KernelEvents::REQUEST_START, 'listeners');
    $eventDispatcher->configListeners($bootstraps);

    $subdirectories = getDirectoryList();
    $componentBootstraps = array();
    foreach ($subdirectories as $folder) {
        $parser->setFilePath($folder . '/config/bootstrap.yml');
        $config = $parser->loadConfig(); 
        if(is_array($config)) {
            $eventDispatcher->configListeners($config);   
        }
        
    }
  
}

function addEvent(array $eventConfig) {
    foreach($eventConfig as $configRow) {
       $event = new core\eventlisteners\Event($configRow['event'], $configRow['listener']); 
    }
    
//$event = new core\eventlisteners\Event(KernelEvents::REQUEST_START, $httpRequest);
}
function getDirectoryList() {
    
    $retval = array();
    if ($handle = opendir(__SITE_PATH . '/src/components')) {
        $blacklist = array('.', '..', 'somedir', 'somefile.php');
        while (false !== ($file = readdir($handle))) {
            if (!in_array($file, $blacklist)) {
                $retval[] = __SITE_PATH . '/src/components/' . $file;
            }
        }
        closedir($handle);
    }

    return $retval;
}
