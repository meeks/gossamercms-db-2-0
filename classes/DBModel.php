<?php


abstract class DBModel extends \ColumnMappings
{
    private $_columns;
    
    
    public function populate($form){
        $this->_columns = array();
        foreach($form as $key => $val){
            $this->_columns[$key] = $val;
        }
    }
    
    
    private function getColumns(){
            
    }
    
    protected function toArray(){
        $data = array();
        $modTime = date('Y-m-d H:i:s');
        foreach ($this->_columns as $column)
        {
            if ($column != 'id'){

                //  Only insert fields that are set
                if (isset($this->_columns[$column])){
                    $data[$column] = $this->_columns[$column];                    
                }

                //  Or named 'created'
                if ($column == 'created')
                    $data[$column] = $modTime;
                if ($column == 'modified')
                    $data[$column] = $modTime;
            }
        }
        return $data;
    }
}
