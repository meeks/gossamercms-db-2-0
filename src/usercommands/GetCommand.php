<?php
namespace usercommands;


use core\commands\GetCommand as CoreGetCommand;

/**
 * Get - used to expose a core Command to the Rest Controller
 */
class GetCommand extends CoreGetCommand
{
	
}
