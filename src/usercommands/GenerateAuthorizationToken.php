<?php
namespace usercommands;

use commands\SaveCommand;
use usercommands\ObserverInterface;
use usercommands\ObservableInterface;
use security\AuthorizationToken;
use libraries\utils\Registry;
use entities\User;


/**
 * GenerateAuthorizationToken - generates a unique token to be validated 
 * and re-issued on EVERY request to sensitive areas (..umm..of the website...not 
 * my skin...)
 * 
 * This is different than the unique login tokens (that never change for that logged in session)
 * whereas these are regenerated on each request to mitigate XSS hack attempts
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
class GenerateAuthorizationToken implements ObserverInterface
{
    private $registry = null;

    private $tableName = null;

    public function executeObservance(ObservableInterface $item, Registry $registry) {
    	
        $this->registry = $registry;

        $this->tableName = $item->getEntity()->getTableName();

         // looks for an observer method with the state name
        if (method_exists($this, $item->getState())) {
            call_user_func_array(array($this, $item->getState()), array($this->registry->User));
        }
    }

	
	
	public function loginSuccess(User $user)
    {

        $token = $this->generateTokenValue($user);

        //first save the information
        $cmd = new SaveCommand(new AuthorizationToken(), $this->registry);
        $cmd->execute(null, array(
            'token' => $token,
            'decaytime' => time(),
            'Users_id' => $user->id,
            'ipAddress' => $_SERVER['REMOTE_ADDR']
        ));

        //now return the new token
        $this->registry->AuthorizationToken = $token;
    }

    private function generateTokenValue(User $user)
    {

        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $userId = $user->id;

        return md5($ipAddress . '_' . $userId . '_' . time());
    }
}
