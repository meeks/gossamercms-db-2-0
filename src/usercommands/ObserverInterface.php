<?php
namespace usercommands;

use usercommands\ObservableInterface;
use libraries\utils\Registry;

interface ObserverInterface
{
    public function executeObservance(ObservableInterface $item, Registry $registry);
}
