<?php

namespace components\blogs\entities;

use entities\AbstractEntity;
use database\SQLInterface;


class Blog extends AbstractEntity implements SQLInterface
{

	public function __construct(){
		$this->primaryKeys = array('id');
		parent::__construct();
	}
}


