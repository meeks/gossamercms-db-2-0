<?php

namespace components\documents\entities;

use entities\AbstractI18nEntity;
use database\SQLInterface;


class Document extends AbstractI18nEntity implements SQLInterface
{

    public function __construct(){
        parent::__construct();
    }
}


