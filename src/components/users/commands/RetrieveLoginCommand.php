<?php
namespace components\users\commands;

use usercommands\AbstractObservableCommand;
use commands\RestInterface;
use usercommands\ObservableInterface;
Use libraries\utils\Registry;
use commands\GetCommand;
use exceptions\ParameterNotPassedException;
use database\SQLInterface;
use entities\User;



/**
 * LoginCommand
 * 
 * @author Dave Meikle
 * 
 * @copyright Quantum Unit Solutions 2014
 */
class LoginCommand extends AbstractObservableCommand implements RestInterface, ObservableInterface
{

    public function __construct(SQLInterface $entity, Registry $registry) {
        
        parent::__construct($entity, $registry);
        
        //you could manually add observers here rather than inside the XML
        // $observers = array(
        // 'usercommands\\SaveUserLoginTime'
        // );
        // $this->setObservers($observers);

        
    }


    public function execute($params = array(), $request = array()) {

        if(!array_key_exists('email',$request)) {
            throw new ParameterNotPassedException('email not specified for login');
        }
        $filter = array(
            'email' => $request['email']
        );

        $user = new User();
        
        $cmd = new GetCommand($user, $this->registry);
        
        $result = $cmd->execute($filter);


        if(is_null($result)){
            $this->setState('loginNotFound', $request);
            return array('error'=>'invalid email');
        }

        $user->populate($result);
        $this->registry->User = $user;
      
        //this is simply an ID token - not really part of the main security system
        //just used on subsequent requests to let us know who this is. We timestamp
        //it to add a slight nudge of complexity - that way no one says "oh yea, 
        // my login token is always 'xxx-xxx-xxxx' - go ahead.. try hacking with it"
        $token = md5($result['id'].'_'.$_SERVER['REMOTE_ADDR'] . '_'. time());
        
        $this->registry->UserToken = $token;
        $this->UserToken = $token;


        //notify any observers to take care of any other issues for us
        $this->setState('loginFound', $token);

        return array('UserToken' => $token);
    }


}