<?php

namespace components\users\entities;

use entities\AbstractEntity;
use database\SQLInterface;


class LoginAccess extends AbstractEntity implements SQLInterface
{
    public function __construct(){
        //override tablename to avoid the extra 's'
        $this->tablename = (get_class($this));
    }
}
