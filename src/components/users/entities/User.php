<?php


namespace components\users\entities;

use entities\AbstractEntity;
use database\SQLInterface;


class User extends AbstractEntity implements SQLInterface
{
	
	public function __construct(){
		$this->primaryKeys = array('id');
		parent::__construct();
	}
}


