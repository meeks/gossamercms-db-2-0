<?php
namespace components\users\listeners;

use core\eventlisteners\AbstractListener;
use commands\SaveCommand;
use libraries\utils\Registry;
use components\users\entities\LoginAccess;

/**
 * SaveUserLoginTime
 * 
 * @author Dave Meikle
 * 
 * @copyright Quantum Unit Solutions 2014
 */
class SaveUserLoginTime extends  AbstractListener
{
    private $registry = null;

    
    private function on_login_success($token) {

        $cmd=new SaveCommand(new LoginAccess(), $this->httpRequest);

        $params = array(
            'userToken' => $token,
            'decayTime'=> strtotime("+20 minutes"),
            'ipAddress' => $_SERVER['REMOTE_ADDR']
        );
        $cmd->execute(null,$params);
    }
}
