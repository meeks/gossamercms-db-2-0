<?php
namespace components\users\observers;

use usercommands\ObserverInterface;
use usercommands\ObservableInterface;
use commands\SaveCommand;
use libraries\utils\Registry;


/**
 * SaveUserLoginTime
 * 
 * @author Dave Meikle
 * 
 * @copyright Quantum Unit Solutions 2014
 */
class IncrementFailedLoginCount implements ObserverInterface
{
    private $registry = null;

    public function executeObservance(ObservableInterface $item, Registry $registry) {
        $this->registry = $registry;

         // looks for an observer method with the state name
        if (method_exists($this, $item->getState())) {
            call_user_func_array(array($this, $item->getState()), array($item->getResult()));
        }
    }

    private function loginFailed($token) {

        $db = new DBConnection();

        $query = 'update users set failedLoginCount = failedLoginCount + 1 where email = ' . $this->registry->params['email'];
        $db->query($query);

        unset($db);
    }
}
