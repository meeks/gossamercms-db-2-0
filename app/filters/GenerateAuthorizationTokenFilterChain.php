<?php
namespace filters;

use filters\FilterChain;



class GenerateAuthorizationTokenFilterChain extends FilterChain
{

    public function processRequest($request, Registry &$registry)
    {
        $this->registry = $registry;
        if (is_null($registry->User)) {
            throw new RuntimeException('User not passed inside registry');
        }

        $token = $this->getNewAuthorizationToken( $registry->User);

        if (strlen($token) > 0) {
            $registry->AuthorizationToken = $token;
        }

        if ($this->successor != null) {
            $this->successor->processRequest($request, $this->registry);
        }

    }


    public function getNewAuthorizationToken(User $user)
    {

        $token = $this->generateTokenValue($user);

        //first save the information
        $cmd = new SaveCommand(new AuthorizationToken(), $this->registry);
        $cmd->execute(null, array(
            'token' => $token,
            'decaytime' => time(),
            'Users_id' => $user->id,
            'ipAddress' => $_SERVER['REMOTE_ADDR']
        ));

        //now return the new token
        return $token;
    }

    private function generateTokenValue(User $user)
    {

        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $userId = $user->id;

        return md5($ipAddress . '_' . $userId . '_' . time());
    }

}
