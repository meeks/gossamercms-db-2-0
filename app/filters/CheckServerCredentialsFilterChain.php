<?php
namespace filters;

use filters\FilterChain;
use libraries\utils\Registry;
use exceptions\InvalidServerIDException;
use exceptions\UnauthorizedAccessException;
use entities\ServerAuthenticationToken;
use commands\GetCommand;

class CheckServerCredentialsFilterChain extends FilterChain
{
    public function __constructor(){

    }

    public function processRequest(array $request, Registry &$registry) {
        ini_set('display_errors', 1);
error_reporting(E_ALL);
        $this->registry = $registry;
        $headers = getallheaders();
error_log(1);
        if (!array_key_exists('serverAuth', $headers)) {
            error_log(2);
            throw new InvalidServerIDException('server identification missing from Headers');
        }
        if(!$this->checkServer($headers['serverAuth'])) {
            error_log(3);
            throw new UnauthorizedAccessException();
        }
error_log('successor');
        if ($this->successor != null) {
            error_log(get_class($this->successor));
            $this->successor->processRequest($request, $this->registry);
        }

    }

    private function checkServer($authToken){

        $token = new ServerAuthenticationToken();
        $cmd = new GetCommand($token, $this->registry);
        $result = $cmd->execute(array('token' => $authToken, 'ipAddress' =>$_SERVER['REMOTE_ADDR']), null);

        if(is_null($result) || count($result) == 0){
            throw new UnauthorizedAccessException("Server not found", 1);
        }

        //check to see if the token is expired - only used if we have a licensing agreement that expires
        // if($result['expirationTime'] < time()) {
            // throw new UnauthorizedAccessException();
        // }

        return ($result['id'] > 0);

    }
}
