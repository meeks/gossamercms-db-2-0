<?php

namespace filters;

use filters\FilterChain;


class LoadApplicationFilterChain extends FilterChain {

	public function processRequest($request, Registry &$registry) {
		$this -> registry = $registry;
		$headers = getallheaders();

		if (!array_key_exists('applicationId', $headers)) {
			throw new ParameterNotPassedException('applicationId missing from Headers');
		} elseif ($application = $this -> getApplication($headers['applicationId'])) {
			$registry -> Application = $application;
		}

		if ($this -> successor != null) {
			$this -> successor -> processRequest($request, $this -> registry);
		}

	}

	private function getApplication($applicationId) {
		$application = new Application();
		$cmd = new GetCommand($application, $this -> registry);
		$result = $cmd -> execute(array('id' => $applicationId), null);

		if (is_null($result) || count($result) == 0) {
			throw new RuntimeException("Application not found", 1);
		}

		foreach ($result as $key => $value) {
			if (is_int($key)) {
				continue;
			}

			$application -> $key = $value;
		}
		return $application;
	}

}
