<?php

namespace filters;

use filters\FilterChain;


/**
 * PostLoginFilterChain Class
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
class PostLoginFilterChain implements ObserverInterface
{
    /**
     * executeObservance - main entry point for generic chaining
     *
     * @param ObservableInterface   item
     * @param Registry              registry
     */
    public function executeObservance(ObservableInterface $item, Registry $registry) {
         // looks for an observer method with the state name
        if (method_exists($this, $item->getState())) {
            call_user_func_array(array($this, $item->getState()), array($registry));
        }
    }

    /**
     * loginSuccess - called after a successfulLogin
     *
     * @param Registry registry
     */
    private function loginSuccess(Registry $registry) {
        if(!isset($registry->Application)) {
            $registry->Application = array('id'=>0);
        }
        $this->loadAllFilters($registry);
    }

    /**
     * loadAllFilters - loads the list of commands to chain through
     *
     * @param Registry registry
     *
     * @return bool|array(error)
     */
    function loadAllFilters(Registry &$registry){


        $xml = new XMLURIParser(__SITE_PATH .'/classes/config/web.xml');
        $chain = $xml->findNodeByURI(__URI, 'actionComplete');

        if(count($chain) < 1) {
            //we have no filters for this page, so send a success to continue on
            return true;
        }

        $filterManager = new FilterChainManager();

        try{
            $filterManager->executeChain($chain, $_REQUEST, $registry);
        }catch(UnauthorizedAccessException $e){
            return array('code' => '401', 'status' => 'Unauthorized');
        }catch(ParameterNotPassedException $e){
            return array('code' => $e->getCode(), 'status' => $e->getMessage());
        }

        return true;
    }
}
