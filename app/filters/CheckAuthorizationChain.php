<?php

namespace filters;

use filters\FilterChain;
use libraries\utils\Registry;
use libraries\utils\Request;
use exceptions\UnauthorizedAccessException;
use entities\User;
use commands\GetCommand;


/**
 * CheckAuthorizationChain Class
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
class CheckAuthorizationChain extends FilterChain
{
    /**
     * processRequest - generic entry point
     *
     * @param array   request
     * @param Registry  registry
     */
	public function processRequest(array $request, Registry &$registry) {
	     $this->registry = $registry;
		 $headers = getallheaders();

         if (is_null($registry->User)) {
            throw new \RuntimeException('User not passed inside registry');
		 } elseif(!$this->verifyAuthorizationToken($headers['Authorization'], $registry->User)) {
			 throw new UnauthorizedAccessException('invalid authorization token');
         }

         if ($this->successor != null) {
             $this->successor->processRequest($request, $this->registry);
         }
    }


    /**
     * verifyAuthorizationToken
     *
     * @param string    token
     * @param User      user
     *
     * @return bool
     */
	 private function verifyAuthorizationToken($token, User $user){

		 $ipAddress = $_SERVER['REMOTE_ADDR'];

		 //first save the information
		 $cmd = new GetCommand(new AuthorizationToken(), $this->registry);
		 $filter = array(
			 'token' => $token,
			 'ipAddress'=>$ipAddress,
			 'Users_id' => $user->id);

		 $result = $cmd->execute($filter);

		 return (!is_null($result) && count($result) > 0);

	 }

}
