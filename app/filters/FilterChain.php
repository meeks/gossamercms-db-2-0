<?php
namespace filters;

use libraries\utils\Registry;
use libraries\utils\Request;

use Monolog\Logger;

/**
 * FilterChain Class - implements the Chain-of-Responsibilty design pattern
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
abstract class FilterChain {

    /**
     * the next link in the chain
     */
    protected $successor;

    /**
     * array of all values passed throughout system
     */
    protected $registry;


    protected $logger = null;
    
    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }
    
    /**
     * property:
     * sets the Successor object
     *
     * @param FilterChain   successor
     */
    public function setSuccessor($successor) {
        $this->successor = $successor;
    }

    /**
     * getAllHeaders
     *
     * @return array    received HTTP Headers
     */
	public function getAllHeaders(){
		$retval = array();

		foreach (getallheaders() as $name => $value) {
		    $retval[$name] = $value;
		}
		return $retval;
	}

    /**
     * processRequest - main entry point for all chain 'link' calls
     *
     * @param Request request
     * @param Registry registry
     */
    abstract public function processRequest(array $request, Registry &$registry);
}