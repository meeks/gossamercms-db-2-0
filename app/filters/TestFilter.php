<?php


namespace filters;

use core\eventlisteners\AbstractListener;
use libraries\utils\Registry;

class TestFilter extends AbstractListener
{
 
    public function processRequest(array $request, Registry &$registry) {
        $this->logger->addDebug('inside testfilter->processRequest');
        echo "inside test filter\r\n";
    }
    
}
