<?php

namespace filters;

use filters\FilterChain;


class LoadUserFilterChain extends FilterChain
{

    public function processRequest($request, Registry &$registry) {
        $this->registry = $registry;
        $headers = getallheaders();

        if (!array_key_exists('userId', $headers)) {
            throw new ParameterNotPassedException('userId missing from Headers');
        } elseif($user = $this->getUser($headers['userId'])){
            $registry->User = $user;

        }

        if ($this->successor != null) {
            $this->successor->processRequest($request, $this->registry);
        }

    }

    private function getUser($userId){
        $user = new User();
        $cmd = new GetCommand($user, $this->registry);
        $result = $cmd->execute(array('id' => $userId), null);

        if(is_null($result) || count($result) == 0){
            throw new RuntimeException("User not found", 1);
        }

        foreach ($result as $key => $value) {
            if(is_int($key)){
                continue;
            }

            $user->$key = $value;
        }
        return $user;
    }
}
