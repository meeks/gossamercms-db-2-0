<?php
namespace controllers;

use controllers\RestController;
/**
 * EntityRestController - used for API configurations that use a Model object
 * to manage the work of user requests
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */

abstract class EntityRestController extends RestController
{
    public function __construct($request, Registry $registry){
        parent::__construct($request, $registry);
    }

	public function execute(){
		if(strlen($this->verb) == 0){
            return;
        }
        if (is_callable($this->entity, $this->verb)) {
            return $this->_response(call_user_func_array(array($this->entity, $this->verb),$this->args));
        }
        return $this->_response('', 400);
	}



}
