<?php
namespace controllers;

use core\http\HTTPRequest;
use libraries\utils\Registry;
use libraries\utils\RequestParameters;
use libraries\utils\Container;

/**
 * REST Controller Class
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
abstract class RestController
{

    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';

    /**
     * Property: entity
     * The Model requested in the URI. eg: /files
     */
    protected $entity = '';

    /**
     * Property: verb
     * An optional additional descriptor about the entity, used for things that can
     * not be handled by the basic methods. eg: /files/process
     */
    protected $verb = '';

    /**
     * Property: args
     * Any additional URI components after the entity and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<entity>/<verb>/<arg0>/<arg1>
     * or /<entity>/<arg0>
     */
    protected $args = Array();

    /**
     * Property: file
     * Stores the input of the PUT request
     */
     protected $file = Null;

     /**
     * Property: command
     * The controller determined by the URI request
     */
     protected $command = null;
     
     /**
      * Property: namespace
      * The namespaced component used that will house the controller (Version 2)
      */
     protected $namespace = null;
     
     protected $componentFolder = null;
     
     protected $component = null;
     
	 protected $registry= null;
     
     protected $container = null;
     
     protected $request = null;

    protected $httpRequest = null;
    /**
     * Constructor: __construct
     * Allow for CORS, assemble and pre-process the data
     */
    public function __construct(HTTPRequest &$request,  Container $container) {

       header("Access-Control-Allow-Origin: *");
       header("Access-Control-Allow-Methods: *");
       header("Content-Type: application/json");

        $this->container = $container;
        $this->httpRequest = $request;
      
        $this->initParams();
        $this->initMethod();
        switch($this->method) {
        case 'DELETE':
        case 'POST':
            $this->request = $this->_cleanInputs($_POST);
            break;
        case 'GET':
            $this->request = $this->_cleanInputs($_GET);
            break;
        case 'PUT':
            $this->request = $this->_cleanInputs($_GET);
            $this->file = file_get_contents("php://input");
            break;
        default:
            $this->_response('Invalid Method', 405);
            break;
        }
       
        $this->httpRequest = new HTTPRequest($this->request);
      
    }

    /**
     * initialize method type based on HTTP_METHOD
     *
     * @param void
     *
     * @return void
     */
    private function initMethod(){
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
    }

    /**
     * initialize parameters based on URI and $_POST
     *
     * @param void
     *
     * @return void
     */
    private function initParams(){

        $request = new RequestParameters();
        $args = parse_url($_SERVER['REQUEST_URI']);
        
        $args['path'] = __URI;
        
        $uriPieces = explode('/',$args['path']);
        array_shift($uriPieces);
        
        $this->componentFolder = array_shift($uriPieces);
        $this->entity = ucfirst(rtrim($this->componentFolder, 's'));

        $this->verb=array_shift($uriPieces);

        $args= explode('/', trim($_SERVER['REQUEST_URI'], '/'));

        $this->getNamespacedCommand();

        $request->setParams($_REQUEST);

        $this->args = $request->getParams();

        unset($request);

        array_shift($this->args);


    }

    private function getNamespacedCommand() {
        
        $filename = 'components\\' . $this->componentFolder . '\\'. ucfirst($this->componentFolder) . 'Component';
    
        if(class_exists($filename )) {
          
            $this->namespace = 'components\\' . $this->componentFolder;  
            $this->component = $filename;
           
        }
            $this->command = ucfirst($this->verb) . 'Command';
     
    }

    /**
     * initializes the response status and encodes results
     *
     * @param array
     * @param int     *
     *
     * @return jsonarray
     */
    protected function _response($data, $status = 200) {
        header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));

        return json_encode($data);
    }

    /**
     * scrubs all user input
     *
     * @param array
     *
     * @return array
     */
    private function _cleanInputs($data) {
    	
        $request = new RequestParameters();
        $data = $request->scrub($data);
        unset($request);

        return $data;
    }

    /**
     *
     * @param int
     *
     * @return string
     */
    private function _requestStatus($code) {
        $status = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($status[$code])?$status[$code]:$status[500];
    }

    /**
     * abstract function to call to start the whole process
     *
     * @param void
     *
     * @return void|variant
     */
	public abstract function execute();

}

