<?php

namespace controllers;

use controllers\RestController;
use libraries\utils\Registry;
use core\http\HTTPRequest;
use libraries\utils\Request;
use exceptions\InterfaceNotImplementedException;
use core\commands\RestInterface;
use core\system\KernelEvents;


/**
 * ActionsRestController - used for API configurations that use a Command object
 * to manage the work of user requests
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
class ActionsRestController extends RestController
{

    /**
     * execute  - the main entry point for the class.
     */
	public function execute(){
		if(strlen($this->verb) == 0){
            return;
        }

		$cmd = null;
        
		$namespacedCommand =((is_null($this->namespace))? 'usercommands\\' . $this->command : $this->namespace . '\\commands\\'. $this->command);
		 
		$namespacedEntity = ((is_null($this->namespace))? 'userentities\\' . $this->entity : $this->namespace . '\\entities\\' . $this->entity);

		$entity = new $namespacedEntity();
        if(is_null($this->component)) {
            $cmd = new $namespacedCommand(new $namespacedEntity, $this->httpRequest); 
             
      // $this->container->get('EventDispatcher')->dispatch(KernelEvents::REQUEST_START, $state);
      
            return $cmd->execute($this->args, $this->request); 
        } else {
            $component = $this->component;  
                        
            $cmd = new $component($namespacedCommand, $namespacedEntity, 'execute',$this->httpRequest->getParameters());  
            $cmd->setContainer($this->container);
              
            return $cmd->handleRequest($this->httpRequest);
        }
        

        if(!$cmd instanceof RestInterface){
            throw new InterfaceNotImplementedException(get_class($cmd) . ' does not implement RestInterface');
        }

//todo - it's looking for execute method... new components may not have that
	}

    
}
