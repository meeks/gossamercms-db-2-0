<?php
namespace entities;

abstract class AbstractEntity
{
	protected $tablename;

	protected $primaryKeys = array();

	public function __construct(){
	    
		$this->tablename = $this->stripNamespacing(get_class($this)) . 's';
	}
    
	private function stripNamespacing($namespacedEntity) {
		$chunks = explode('\\', $namespacedEntity);
		
		return array_pop($chunks);
	}

	public function getTableName(){
		return $this->tablename;
	}

	public function getPrimaryKeys(){
		return $this->primaryKeys;
	}


    public function populate($params = array()){
        foreach ($params as $key => $value) {
            if(is_int($key)){
                continue;
            }

            $this->$key = $value;
        }
    }
}
