<?php
namespace entities;

use entities\AbstractEntity;
use database\SQLInterface;

class Application extends AbstractEntity implements SQLInterface
{
	
	public function __construct(){
		$this->primaryKeys = array('id');
		parent::__construct();
	}
}


