<?php
namespace core;

class Config
{
    private $configuration;
    
    public function __construct($configuration = array()){
        $this->configuration = $configuration;
    }
    
    public function get($item){
        if(array_key_exists($item, $this->configuration)){
            return $this->configuration[$item];
        }
        
        return null;
    }
    
    public function set($item, $params){
        $this->configuration[$item] = $params;
    }
    
    public function toArray(){
        return $this->configuration;
    }
}
