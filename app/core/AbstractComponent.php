<?php


namespace core;


use libraries\utils\YAMLParser;
use libraries\service\FilterChainManager;
use libraries\utils\filters\AbstractFilter;
use exceptions\ParameterNotPassedException;
use exceptions\UnauthorizedAccessException;
use core\http\HTTPRequest;
use core\http\HTTPResponse;
use libraries\utils\Registry;
use core\commands\RestInterface;
use core\eventlisteners\EventDispatcher;
use Monolog\Logger;
use libraries\utils\Container;



/**
 * 
 * class AbstractComponent -    this is the base class for the drop in components used to
 *                              preload any filters for the selected component as well as 
 *                              any pre-config.
 * 
 * @author Dave Meikle
 * 
 * @Copyright: Quantum Unit Solutions 2014
 */

abstract class AbstractComponent extends YAMLParser implements RestInterface
{
    private $command = null;
    
    private $entity = null;
    
    private $method = null;
    
    private $params = null;
    
    private $filters = null;
    
    private $observers = null;
    
    private $container = null;
    
    public function __construct($command, $entity, $method = null, array $params = null) {
        //$this->logger->addDebug("abstractComponent: command:$command  entity:$entity" );

        if(is_null($command)) {
            throw new ParameterNotPassedException('command is null');
        }else if(is_null($entity)) {
            throw new ParameterNotPassedException('entity is null');
        }
        $this->command = $command;
          
        $this->entity = $entity;
          
        $this->method = $method;
          
        $this->params = $params;
        
      //  $this->loadConfig();
    }
    
    public function setContainer(Container $container) {
        $this->container = $container;
    }
    
    
    
    
    /**
     * handleRequest - entry point for the class
     * 
     * @param Request   the filtered request object
     * @param Registry  the registry object
     * 
     */
    public function handleRequest(HTTPRequest &$request) {
       
        $handler = array(
            $this->command,
            $this->method
        );
      
        //$registry->modelName = $this->model;
        //$registry->Model = $this->entity;
        $entity = new $this->entity();
      
        if (is_callable($handler)) {
            
            $commandName = $this->command;
         
            $command = new $commandName($entity, $request);

            return call_user_func_array(array(
                $command,
                $this->method
            ), !isset($this->params) ? array() : $this->params);
        }       
    }
    
    /** the __NAMESPACE__ is determined at compile time so we need to place this in the child:
     * return str_replace('\\', DIRECTORY_SEPARATOR, __NAMESPACE__);
     */
    protected abstract function getChildNamespace();
    
    /**
     * runFilters - executes the filter chain based on the component custom web.xml configuration
     */
    private function runFilters(Request $request, Registry $registry) {
       $filterManager = new FilterChainManager();

        try{
            $filterManager->executeChain($this->filters, $_REQUEST, $registry);    
        }catch(UnauthorizedAccessException $e){error_log('unauth');
            //by returning this to the client they should configured their
            //system to respond with a subsequent login request, followed
            //by repeating their original request
            return array('code' => '401', 'status' => 'Unauthorized');
        }catch(ParameterNotPassedException $e){error_log('no param');
            return array('code' => $e->getCode(), 'status' => $e->getMessage());
        }
    
        return true;
    }
}
