<?php

namespace core\system;

use core\http\HTTPRequest;
use core\http\HTTPResponse;
use core\eventlisteners\Event;
use core\system\KernelEvents;
use controllers\ActionsRestController;
use components\users\commands\LoginCommand;
use core\http\ResponseFormatter;
use libraries\utils\YAMLConfiguration;
use libraries\utils\Container;
use Monolog\Logger;



class Kernel
{
    private $container = null;
   
    private $logger = null;
   
    public function __construct(Container $container, Logger $logger) {
        $this->container = $container;
        $this->logger = $logger;
    }
    public function run() {
        
        $httpRequest = new HTTPRequest($_REQUEST);
        $httpResponse = new HTTPResponse;
        
        
        //EventDispatcher is created in bootstrap
        //first, run any security checks before starting the request
        $event = new Event(KernelEvents::ENTRY_POINT, $httpRequest);
        
        $this->container->get('EventDispatcher')->dispatch('all', KernelEvents::REQUEST_START);
        
        //still here? ok, now start the request
        $event = new Event(KernelEvents::REQUEST_START, $httpRequest);
        
        $this->container->get('EventDispatcher')->dispatch(__YML_KEY, KernelEvents::REQUEST_START);
        
        $this->logger->addDebug('dispatcher started in index - state set to ' . KernelEvents::REQUEST_START);
        
        
        //use the ActionsRestController if the Command objects handle the requests
        //and the entity objects are empty containers
        $rest = new ActionsRestController($httpRequest, $this->container);
        
        $this->logger->addDebug('ActionsRestController instantiated');
        
        //use the EntityRestController if the Model objects handle the requests
        //and the command objects are empty containers - not currently implemented in
        //this demo API. Placeholder only.
        //$rest = new EntityRestController($_REQUEST, $registry);
        
        
        
        try{
            $httpRequest = $rest->execute();
            $this->container->get('EventDispatcher')->dispatch(KernelEvents::REQUEST_END, 'request_success');
           
        }catch(\Exception $e){
            $this->logger->addError('error during rest->execute:');
            $this->logger->addError($e->getMessage());
            $this->container->get('EventDispatcher')->dispatch(KernelEvents::EXCEPTION, 'error');
            header("HTTP/1.1 " . $e->getCode() . " " . $e->getMessage());
            echo json_encode(array('code' => $e->getCode(), 'message' => $e->getMessage()));
            exit;
        }
        
        unset($rest);
        $this->container->get('EventDispatcher')->dispatch(KernelEvents::RESPONSE_START, 'response_begin');
        
        $responseConfigs = new YAMLConfiguration($this->logger, 'bootstrap');
        
        if(!is_null($httpRequest)) {    
            //cherry pick the registry for items we want to expose in our result
            ResponseFormatter::format($responseConfigs, $httpRequest, $httpResponse, $this->logger);
        }
        
        $this->container->get('EventDispatcher')->dispatch(KernelEvents::RESPONSE_END, 'response_end');
        
        echo json_encode($httpResponse->getContent());
        
        $this->container->get('EventDispatcher')->dispatch(KernelEvents::TERMINATE, 'terminate');
    }
}
