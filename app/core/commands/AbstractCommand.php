<?php
namespace core\commands;

use database\DBConnection;
use database\SQLInterface;
use libraries\service\QueryBuilderManager;
use core\http\HTTPRequest;
use core\http\HTTPResponse;
use libraries\utils\Container;

/**
 * Abstract Command Class extending from Database Connection
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
abstract class AbstractCommand extends DBConnection
{
    /**
     * SQLInterface Object mapped to a database table
     * */
    protected $entity = null;

    /**
     * Registry object for passing parameters throughout the system
     */
	protected $registry = null;

    /**
     * QueryBuilder for generating all queries, standard and I18n
     */
	private $queryBuilder = null;

    protected $httpRequest = null;
    
    protected $response = null;
    
    /**
     * Constructor
     *
     * @param SQLInterfce   $entity
     * @param Registry      $registry
     */
    public function __construct(SQLInterface $entity, HTTPRequest &$request){

        if(!$entity instanceof SQLInterface){
            throw new \InvalidArgumentException('Object does not implement SQLInterface');
        }
        
        $this->httpRequest = $request;

        $this->entity = $entity;

		$this->queryBuilder = new QueryBuilderManager(array());

    }

    public function setContainer(Container $container) {
        $this->container = $container;
    }
    
    public function getContainer() {
        return $this->container;
    }
    
    public function getEntity() {
        return $this->entity;
    }
    

    /**
     * @return string query
     */
	protected function getQueryBuilder(){
		return $this->queryBuilder;
	}

    /**
     * executes code specific to the child class
     *
     * @param array     URI params
     * @param array     POST params
     */
    public abstract function execute($params = array(), $request = array());

    public function getHttpRequest() {
        return $this->httpRequest;
    }

}
