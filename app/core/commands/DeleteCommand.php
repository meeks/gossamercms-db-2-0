<?php
namespace core\commands;

use core\commands\AbstractCommand;
use core\commands\RestInterface;
use libraries\service\QueryBuilderManager;

/**
 * Delete Command Class
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
class DeleteCommand extends AbstractCommand implements  RestInterface
{

    /**
     * Deletes an entity row from the database
     *
     * @param array     URI params
     * @param array     POST params
     */
	public function execute($params = array(), $requestParams = array()){
        $this->beginTransaction();

        try{
            //first delete the child tables
            $this->deleteI18nLocales($params);

            //now delete the main tables
    		$this->getQueryBuilder()->where($params);
    		$firstResult = $this->query($this->getQueryBuilder()->getQuery($this->entity, QueryBuilderManager::DELETE_QUERY));

    		$this->commitTransaction();
        }catch(Exception $e){
            $this->rollbackTransaction();
        }

        $this->httpRequest->setAttribute('deleteResult', $firstResult);
		return $this->httpRequest;

	}

    /**
     * deletes a row from the I18n table
     *
     * @param array     URI params
     */
    private function deleteI18nLocales($params) {

        $filter = array($this->entity->getI18nIdentifier() => $params['id']);

        $this->getQueryBuilder()->where($filter);

        $this->query($this->getQueryBuilder()->getQuery($this->entity, QueryBuilderManager::DELETE_QUERY, QueryBuilderManager::CHILD_ONLY));
    }
}
