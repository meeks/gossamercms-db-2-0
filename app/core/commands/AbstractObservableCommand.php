<?php
namespace core\commands;

use core\commands\AbstractCommand;
use database\SQLInterface;
use libraries\utils\Registry;
use usercommands\ObservableInterface;
use usercommands\ObserverInterface;

/**
 * AbstractObserverCommand Class
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
abstract class AbstractObservableCommand extends AbstractCommand{

    /**
     * list of observers
     */
    protected $observers = array();

    /**
     * callable handle to method upon completion
     */
    protected $state;

    /**
     * result of execution from child classes
     */
    protected $result;

    /**
     * Registry object for storing system wide variables
     */
    protected $registry;

    public function __construct(SQLInterface $entity, Registry $registry){
       
        parent::__construct($entity, $registry);

	
		
    }

   

    protected function setState($state, $result){
        $this->container->get('EventDispatcher')->dispatch(__YML_KEY, $state, $result);
    }

    public function getRegistry() {
        return $this->registry;
    }

    public function setRegistry(Registry $registry){
        $this->registry = $registry;
    }
}
