<?php
namespace core\commands;

use core\commands\AbstractCommand;
use core\commands\RestInterface;
use libraries\service\QueryBuilderManager;
use entities\AbstractI18nEntity;
use entities\AbstractEntity;


/**
 * Save Command Class
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
class GetCommand extends AbstractCommand implements  RestInterface
{

    /**
     * retrieves a single row from the database
     *
     * @param array     URI params
     * @param array     POST params
     */
    public function execute($params = array(), $request = array())
    {
        $this->getQueryBuilder()->where($this->httpRequest->getParameters());
		$query = $this->getQueryBuilder()->getQuery($this->entity, QueryBuilderManager::GET_ITEM_QUERY);

//$this->logger->addDebug($query);
        $firstResult = $this->query($query);

        if (is_array($firstResult) && count($firstResult) > 0) {

            $this->loadI18nValues($firstResult, $params);
            $entityName = get_class($this->entity);
      
            $this->httpRequest->setAttribute($entityName, $firstResult[0]);
           // $this->registry->$entityName = $firstResult[0];

            return $this->httpRequest;
        }

        return null;
    }

    /**
     * load child rows from I18n specific to requested entity
     */
    private function loadI18nValues(&$firstResult, $params)
    {
        if (!$this->entity instanceof AbstractI18nEntity) {

            return;
        }
        $filter = array($this->entity->getI18nIdentifier() => $firstResult[0]['id']);
        $localesKey = 'locales';

        if(array_key_exists('locale', $params)){
            $filter['locale'] = $params['locale'];
            $localesKey = $params['locale'];
        }

        $this->getQueryBuilder()->where($filter);

        $i18nResult = $this->query($this->getQueryBuilder()->getQuery($this->entity, QueryBuilderManager::GET_ALL_ITEMS_QUERY, QueryBuilderManager::CHILD_ONLY));

        $firstResult[0]['locales'][$localesKey] = $i18nResult;
    }

}
