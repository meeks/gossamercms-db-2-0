<?php
namespace core\commands;

use core\commands\AbstractCommand;
use core\commands\RestInterface;
use libraries\service\QueryBuilderManager;

class SaveCommand extends AbstractCommand implements  RestInterface
{

    /**
     * saves an entity into the database
     *
     * @param array     URI params
     * @param array     POST params
     */
	public function execute($params = array(), $requestParams = array()){

		$this->getQueryBuilder()->setValues($requestParams);
		$this->getQueryBuilder()->where($params);

		$query = $this->getQueryBuilder()->getQuery($this->entity, QueryBuilderManager::SAVE_QUERY, QueryBuilderManager::PARENT_ONLY);

        $this->beginTransaction();
        try{
            //first save into the parent table
            $result = $this->query("$query", FALSE);

            if(intval($result) > 0) {
                $this->saveI18nValues($result, $params, $requestParams, $params['locale']);
            }elseif(intval($requestParams['id']) > 0){
                if(!is_array($params['locale'])){
                    $this->saveI18nValues($requestParams['id'], $params, $requestParams, $params['locale']);
                }else{
                    foreach ($params['locale'] as $locale) {
                        $this->saveI18nValues($requestParams['id'], $params, $requestParams, $locale);
                    }
                }

            }
            $this->commitTransaction();
        }catch(Exception $e){
            $this->rollbackTransaction();
        }
    
        $this->httpRequest->setAttribute('save', $result);
        return $this->httpRequest;
	}

    /**
     * save child rows into I18n specific to saved entity
     */
    private function saveI18nValues($firstResult, $params, $requestParams, $locale) {
        if(!$this->entity instanceof AbstractI18nEntity) {

            return;
        }

        $params = array($this->entity->getI18nIdentifier() => $firstResult, 'locale' => $locale);

        //need to add this, since it is what associates us to the parent table
        $requestParams[$this->entity->getI18nIdentifier()] = $firstResult;

        $this->getQueryBuilder()->setValues($requestParams);
        $this->getQueryBuilder()->where($params);

        $this->query($this->getQueryBuilder()->getQuery($this->entity, QueryBuilderManager::SAVE_QUERY, QueryBuilderManager::CHILD_ONLY));

    }
}
