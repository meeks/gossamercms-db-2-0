<?php
namespace core\commands;

use core\commands\AbstractCommand;
use core\commands\RestInterface;
use libraries\service\QueryBuilderManager;
use entities\AbstractI18nEntity;
use entities\AbstractEntity;
use core\http\HTTPRequest;

/**
 * List All Command Class
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
class ListCommand extends AbstractCommand implements  RestInterface
{

    /**
     * retrieves a multiple rows from the database
     *
     * @param array     URI params
     * @param array     POST params
     */
	public function execute($params = array(), $request = array()){

         $this->getQueryBuilder()->where($this->httpRequest->getParameters());

		 $result = $this->query($this->getQueryBuilder()->getQuery($this->entity, QueryBuilderManager::GET_ALL_ITEMS_QUERY, QueryBuilderManager::PARENT_AND_CHILD));
         $param = get_class($this->entity) . 's';
      
         $this->httpRequest->setAttribute($param, $result);
       
		return $this->httpRequest;
	}

}
