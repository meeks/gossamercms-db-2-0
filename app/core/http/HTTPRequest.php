<?php

namespace core\http;

use exceptions\InvalidServerIdException;


class HTTPRequest extends AbstractHTTP
{
    
    
    protected $requestParameters = null;
    
    protected $parameters = array();
    
    public function __construct($requestParameters = null) {
        
        $this->requestParameters = $requestParameters; 
        //remove the uri from the params
        unset($this->requestParameters[$_SERVER['REQUEST_URI']]);  
                
    }
    
    public function getHeader($headerName) {
        return $this->headers[$headerName];
    }
    private function init() {
     
        $this->headers = getallheaders();
        $this->attributes['ipAddress'] = $_SERVER['REMOTE_ADDR'];

    }
    
    
    public function setAttribute($key, $value) {
       
        $this->attributes[$key] = $value;
    }
    
    public function getAttribute($key) {
        if(array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }
        
        return null;
    }
    
    public function getParameters() {
        return $this->requestParameters;
    }
    
   
}


