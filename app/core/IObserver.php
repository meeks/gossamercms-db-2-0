<?php

namespace core\IObserver;

interface IObserver 
{
    public function executeObservance($state, $result);
}
