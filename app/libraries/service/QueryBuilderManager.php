<?php
namespace libraries\service;

use libraries\service\ManagerInterface;
use database\SQLInterface;
use entities\AbstractEntity;
use entities\AbstractI18nEntity;
use database\DBConnection;
use libraries\service\ColumnMappings;

class QueryBuilderManager implements ManagerInterface
{

    const SAVE_QUERY = 'save';

    const DELETE_QUERY = 'delete';

    const GET_ITEM_QUERY = 'get';

    const GET_ALL_ITEMS_QUERY = 'getall';

    const PARENT_ONLY = 'parentOnly';

    const CHILD_ONLY  =  'childOnly';

    const PARENT_AND_CHILD = 'parentAndChild';

    private $fields = null;//derived from passed in array

    private $fieldNames = array();//derived from values

    private $values = null;

    private $andFilter = null;

    private $orFilter = null;

    private $offset = 0;

    private $limit = 0;

    private $concatenator = ' AND ';

    private $tableName = '';

    private $primaryKeys = null;

    private $tableColumns = null;

    private $i18nJoin = null;

    private $orderBy = null;

    private $dbConnection = null;

    public function __construct($injectables = array()){
        if(array_key_exists('dbConnection', $injectables)) {
            //perhaps using a project db
            $this->dbConnection = $injectables['dbConnection'];
        }

    }

    private function setTablename(SQLInterface $entity, $i18nQueryType){
        if(self::CHILD_ONLY == $i18nQueryType && $entity instanceof AbstractI18nEntity) {
            $this->tableName = $entity->getI18nTablename();
        }else{
            $this->tableName = $entity->getTableName();
        }
    }

    private function setPrimaryKeys(SQLInterface $entity, $i18nQueryType) {
        if(!is_null($i18nQueryType) && $entity instanceof AbstractI18nEntity) {
            $this->primaryKeys = $entity->getI18nPrimaryKeys();
        } else {
            $this->primaryKeys = $entity->getPrimaryKeys();
        }
    }

    private function init(SQLInterface $entity, $i18nQueryType, $queryType) {
        $this->tableColumns = null;
        $this->fieldNames = null;
        $this->fields = null;

        $this->setTablename($entity, $i18nQueryType);


        $this->setPrimaryKeys($entity, $i18nQueryType);

        $this->loadActualColumns($entity, $i18nQueryType);

    }

    public function getQuery(SQLInterface $entity, $queryType = 'getall', $i18nQueryType = null){
        if(!$entity instanceof SQLInterface){
            throw new \RuntimeException('entity must implement SQLInterface');
        }

        $this->init($entity, $i18nQueryType, $queryType);
        $query='';
        if($queryType == self::DELETE_QUERY){
            $query = $this->buildDeleteStatement();
        }elseif($queryType == self::SAVE_QUERY){
            $query = $this->buildSaveStatement();
        }elseif($queryType == self::GET_ITEM_QUERY){
            $query = $this->buildSelectStatement(true, $entity);
        }elseif($queryType == self::GET_ALL_ITEMS_QUERY){
            $query = $this->buildSelectStatement(false, $entity);
        }

        return preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $query);
    }


    private function getDBConnection() {

        //did we NOT receive an overriding connection? ok, create a default instance
        if(is_null($this->dbConnection)) {

            $this->dbConnection = new DBConnection();
        }

        return $this->dbConnection;
    }

    private function loadActualColumns(SQLInterface $entity, $i18nQueryType){

        $dbConnection = $this->getDBConnection();

        $columnMappings = new ColumnMappings($dbConnection);

        if(!$entity instanceof AbstractI18nEntity || (is_null($i18nQueryType))) {

            $this->tableColumns = $columnMappings->getTableColumnList($entity->getTableName());
        }elseif(($entity instanceof AbstractI18nEntity) && self::PARENT_AND_CHILD == $i18nQueryType) {


            //used for select statements only, but needs to join across tables
            $this->tableColumns = $columnMappings->getTableColumnList($entity->getTableName());
             //if it's an i18n select then query the locales table too
            $this->tableColumns = array_merge($this->tableColumns, $columnMappings->getTableColumnList($entity->getI18nTablename()));
            //set this flag so we can call it later
            $this->i18nJoin = $this->joinI18nTable($entity);
        }elseif(($entity instanceof AbstractI18nEntity) && self::CHILD_ONLY == $i18nQueryType) {


             $this->tableColumns = $columnMappings->getTableColumnList($entity->getI18nTablename());
        }elseif(($entity instanceof AbstractI18nEntity) && self::PARENT_ONLY == $i18nQueryType) {


            $this->tableColumns = $columnMappings->getTableColumnList($entity->getTableName());
        }


        unset ($columnMappings);
    }

    private function joinI18nTable(SQLInterface $entity){

       return ' JOIN ' . $entity->getI18nTablename() . ' ON ' . $entity->getTableName() . '.id = ' . $entity->getI18nTablename() .
        '.' . $entity->getI18nIdentifier();
    }

    public function where($filter){

        $this->andFilter = $filter;
       
    }

    public function getWhere(){
        return $this->getWhereStatement();
    }
    public function orWhere($filter){
        $this->orFilter = $filter;
    }

    public function setValues($values){
        $this->values = $values;
    }

    public function setFields($fields){
        $this->fields = $fields;
    }

    private function buildSelectStatement($firstRowOnly = false, SQLInterface $entity){
        $select = 'SELECT ';

        if(!is_null($this->fields)){
            $select .= implode(',' , $this->fields);
        }else{
            $select .= '*';
        }

        $select .= ' FROM ' .$this->tableName;
        if(!is_null($this->i18nJoin)){
            $select .= $this->i18nJoin;
        }

        $select .= $this->getWhereStatement();

        $this->parseDirectives();

        $select .= $this->getOrderBy();

        $select .= $this->getOffset($firstRowOnly);

        return $select;
    }

    private function getOrderBy() {
        if(!is_null($this->orderBy)) {
            return $this->orderBy;
        }
    }
    public function joinTable($tablenameToJoin, $columnsToJoinOn = array()){

        return ' JOIN ' . $tablenameToJoin . ' ON ' . $columnsToJoinOn[0] . ' = ' . $columnsToJoinOn[1];
    }

    private function getOffset($firstRowOnly=false){
        if($firstRowOnly){
            return ' LIMIT 1';
        }
        if($this->limit > 0){
            return ' LIMIT ' . $this->offset . ',' . $this->limit;
        }
        return '';
    }

    private function getWhereStatement(){

        if(is_null($this->andFilter) && is_null($this->orFilter)){
            return '';
        }

        $where = ' WHERE ';
        $hasFilter = false;

        $andWhere = $this->buildAndWhereFilter();

        $orWhere = $this->buildOrWhereFilter();

        if(strlen($andWhere) > 0){
            $where .= $andWhere;
            $hasFilter = true;
            
        }
        if(strlen($orWhere) > 0){
            if($hasFilter){

            }
            $where .= (($hasFilter) ? ' OR ':'') . $orWhere;
            $hasFilter = true;
        }

        return ($hasFilter) ? $where :'';
    }

    private function buildAndWhereFilter(){

        if(is_null($this->andFilter) || count($this->andFilter) == 0){
            return '';
        }

        //method has passed in a hard string for filtering
        if(!is_null($this->andFilter) && !is_array($this->andFilter)){
            return $this->andFilter;
        }

        $where = '';
        
        foreach($this->andFilter as $key => $val){
            if(!in_array($key,$this->tableColumns)){
                continue;
            }
            $where .= ' AND (`' . $key. '` = \'' . ($val).'\')';
        }

        return '(' . substr($where, 4) . ')';
    }

    private function buildOrWhereFilter(){
        if(is_null($this->orFilter) || count($this->orFilter) == 0){
            return '';
        }
        //method has passed in a hard string for filtering
        if(!is_null($this->orFilter) && !is_array($this->orFilter)){
            return $this->orFilter;
        }

        $where = '';
        foreach($this->orFilter as $key => $val){
            if(!in_array($key,$this->tableColumns)){
                continue;
            }
            $where .= ' OR (`' . $key. '` = \'' . ($val).'\')';
        }

        return '(' . substr($where, 3) . ')';
    }

    private function buildDeleteStatement(){
        return 'DElETE FROM ' . $this->tableName . $this->getWhereStatement();
    }

    private function buildSaveStatement(){
        //insert into table
        $query = 'INSERT INTO ' . $this->tableName;

        //need to parse values first since this will configure our matching field names
        $values = $this->parseValuesToInsert();

        //(col1,col2,col3)
        $fieldNames = $this->parseFieldNames();

        $query .= $fieldNames . $values;

        //this would normally be enough, but we rely on primary key for detecting updates
        $query .= ' ON DUPLICATE KEY UPDATE ' . $this->buildUpdateStatement();

        return $query;
    }

    private function buildUpdateStatement(){
        $values = '';
        foreach($this->values as $key => $val){
            //don't try to update primary keys
            if(in_array($key, $this->primaryKeys)) {
                continue;
            }
            //only accept columns that exist in the table
            if(!in_array($key,$this->tableColumns)){
                continue;
            }
            $values .= ', `'. $key . '` = \'' . ($val).'\'';
        }

        return substr($values, 1);
    }

    private function parseValuesToInsert(){
        $values = '';

        foreach ($this->values as $key => $value) {

            if(!in_array($key,$this->tableColumns)){
                continue;
            }
            $this->fieldNames[] = $key;

            $values .= ', \'' . $value . '\'';


        }

        return ' VALUES (' . substr($values,1) . ')';
    }

    private function parseDirectives() {

        foreach($this->andFilter as $key => $value){

            if(strpos($key, 'directive::')=== FALSE ) {
                continue;
            }

            if('directive::ORDER_BY' == $key){
                $this->setOrderBy($value);
            }
            if('directive::LIMIT' == $key) {
                $this->setLimit($this->andFilter['directive::OFFSET'], $this->andFilter['directive::LIMIT']);
            }
        }
    }

    private function setOrderBy($columnAndDirection) {
        $this->orderBy = ' order by ' . $columnAndDirection;
    }


    private function setLimit($offset, $limit) {
        $this->offset = $offset;
        $this->limit = $limit;
    }

    private function parseFieldNames(){
        if(is_null($this->fields)){
            return '(`' . implode('`,`',$this->fieldNames) . '`)';
        }
        if(!is_array($this->fields)){
            return '(`' . $this->fields .'`)';
        }
        return '(`' . implode('`,`',$this->fields) . '`)';
    }


}

