<?php
namespace libraries\service;

use libraries\utils\Registry;
use filters\FilterChain;

/**
 * FilterChainManager Class
 *
 * Author: Dave Meikle
 * Copyright: Quantum Unit Solutions 2013
 */
class FilterChainManager
{

    /**
     * executeChain - iterates the list of objects and calls each one
     *
     * @param array     chain - list of objects
     * @param Registry  registry - array of values to pass throughout system
     *
     * @return Registry
     */
	public function executeChain($chain = array(), $request, Registry $registry) {
	    if(count($chain) < 1) {
            return registry;
        }
       
		$lastObject = null;
		//we need to know the successor before the predecessor
		//so reverse the chain before traversing
		$reverseChain = array_reverse($chain);

		foreach($reverseChain as $link) {

            try{
               
               $object = new $link();

            }catch(\Exception $e){
                error_log($e->getMessage());
            }

			if(!$object instanceof FilterChain) {
				throw new \RuntimeException($link . ' must extend FilterChain');
			}

			if(!is_null($lastObject)) {
				$object->setSuccessor($lastObject);
			}

			$lastObject = $object;

		}

		try{
		    if(!is_null($lastObject)) {
		        $lastObject->processRequest($request, $registry);
		    }
        } catch(\Exception $e) {
            error_log($e->getCode());
            throw $e;
        }

        //return the modified registry in case we need to utilize anything from within
        return $registry;
    }
}
