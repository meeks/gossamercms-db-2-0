<?php


namespace libraries\service;

use core\eventlisteners\AbstractListener;

class TestAnotherListener extends AbstractListener
{
    
    
    protected function sayHello($params) {
     echo "<br>what? another listerns</br>\r\n";   
    }
    
    protected function on_request_start($params) {
        $this->logger->addDebug('on request start called in TestAnotherListener');
    }
}
