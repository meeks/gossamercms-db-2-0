<?php
namespace libraries\service;

use libraries\service\ManagerInterface;
use database\DBConnection;

class LoginManager implements  ManagerInterface
{
    private $dbConnection = null;
    
    public function __construct($injectables = array()){
        if(!array_key_exists('db', $injectables)){
            throw new \RuntimeException('Database Connection must be specified in constructor');
        }elseif(!$injectables['db'] instanceof \DBConnection){
            throw new \RuntimeException('db passed in must be instance of DBConnection');
        }        
        
        $this->dbConnection = $injectables['db'];
    }
    
    
    public function validateToken($token){
        $result = $this->dbConnection->query('select Users_id as id from Tokens where token = \'' . $token .'\' limit 1');
        
        if(is_array($result) && count($result) > 0){
            //now we need to invalidate this token so that its not re-used
            $this->dbConnection->query('delete from Tokens where token=\'' . $token . '\' and Users_id = \'' . $result['id'] . '\'');
          
            return $result['id'];
        }
        
        return null;
        
        
    }
}
