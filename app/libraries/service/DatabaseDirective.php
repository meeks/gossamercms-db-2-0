<?php

class DatabaseDirective
{
    const ORDER_BY = ' order by ';

    const DIRECTION_ASC = ' asc ';

    const DIRECTION_DESC = ' desc ';

    const LIMIT = ' limit ';

    const OFFSET = ' offset ';

    private $directives = null;

    public function setDirective($directive = array()) {
        if(is_null($this->directives)) {
            $this->directives = array();
        }
        array_push($this->directives, $directive);
    }

    public function getDirectives() {
        return $this->directives;
    }


    public function serialize(){
        return json_encode($this->directives);
    }
}
