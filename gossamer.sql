-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: gossamer
-- ------------------------------------------------------
-- Server version	5.1.73-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AllowableIPAddresses`
--

DROP TABLE IF EXISTS `AllowableIPAddresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AllowableIPAddresses` (
  `ipAddress` varchar(15) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `serverName` varchar(30) NOT NULL,
  `Hosts_id` int(11) NOT NULL,
  PRIMARY KEY (`ipAddress`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AllowableIPAddresses`
--

LOCK TABLES `AllowableIPAddresses` WRITE;
/*!40000 ALTER TABLE `AllowableIPAddresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `AllowableIPAddresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AnnouncementCategories`
--

DROP TABLE IF EXISTS `AnnouncementCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnnouncementCategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `lastModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnnouncementCategories`
--

LOCK TABLES `AnnouncementCategories` WRITE;
/*!40000 ALTER TABLE `AnnouncementCategories` DISABLE KEYS */;
INSERT INTO `AnnouncementCategories` VALUES (1,'General Announcement',0,0,'2013-12-08 21:26:58',1),(2,'Important Update',0,0,'2013-12-08 21:27:19',1);
/*!40000 ALTER TABLE `AnnouncementCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AuthorizationTokens`
--

DROP TABLE IF EXISTS `AuthorizationTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuthorizationTokens` (
  `token` varchar(40) DEFAULT NULL,
  `decayTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ipAddress` varchar(15) NOT NULL,
  `Users_id` int(11) NOT NULL,
  `Applications_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ipAddress`,`Users_id`,`Applications_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AuthorizationTokens`
--

LOCK TABLES `AuthorizationTokens` WRITE;
/*!40000 ALTER TABLE `AuthorizationTokens` DISABLE KEYS */;
INSERT INTO `AuthorizationTokens` VALUES ('83a5d4cda616e2d5c941a34ebd4a4ade','2013-12-03 21:01:45','162.156.9.8',1,0),('472260c6c5029fda7983fee8f47ec5f7','2013-12-03 21:03:13','72.4.146.128',1,0),('7a8346a65c15811984f44924c84b37a3','2013-12-07 06:29:09','72.4.146.128',7,0);
/*!40000 ALTER TABLE `AuthorizationTokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Blogs`
--

DROP TABLE IF EXISTS `Blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Authors_id` int(11) DEFAULT NULL,
  `Categories_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `content` text,
  `dateEntered` date DEFAULT NULL,
  `lastModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `numViews` int(11) NOT NULL DEFAULT '0',
  `rating` float NOT NULL DEFAULT '0',
  `isActive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Blogs`
--

LOCK TABLES `Blogs` WRITE;
/*!40000 ALTER TABLE `Blogs` DISABLE KEYS */;
INSERT INTO `Blogs` VALUES (1,1,1,'Easiest Way to Display Errors in PHP','','<p>\r\n    OK, so here`s a chunk of code I always simply google whenever I`m debugging PHP if I don`t feel like locating a file that has it commented out. I tend to remove the debug attributes simply to keep my pages nice and clean.\r\n</p>\r\n<p>\r\nFor those of you looking for it, the easiest way is:<br/>\r\n<strong>ini_set(`display_errors`, 1); <br/>\r\nerror_reporting(E_ALL);</strong>\r\n</p>\r\n<p>\r\nSimply put that at the top of the page and let it rip. You`ll be able to see a nice...uh... well... you`ll hopefully be a step closer to deciphering what`s going on...    \r\n</p>\r\n\r\n<p>\r\nAnother more elegant approach is to use a verbose log it so you can go through it later. That can be achieved like this:<br/>\r\n<strong>ini_set(`display_errors`, 1); <br/>\r\nini_set(`log_errors`, 1); <br/>\r\nini_set(`error_log`, dirname(__FILE__) . `/error_log.txt`); <br/>\r\nerror_reporting(E_ALL);</strong></p>','2012-02-04','2014-04-15 03:56:09',1053,0,1),(2,NULL,NULL,'C#\'s Version of AJAX Not Really Impressive For the Hand Coder','','C# has had their own version of AJAX for quite a few years now.  Works real well too - as long as you\'re developing your web apps as a drag n drop programmer using the tools provided by Microsoft\'s Dot Net architecture. Forces you to play inside their sandbox and use their controls although there are still methods of writing in the back end that don\'t necessarily call on the Microsoft page controls - not everyone wants to use the Dot Net TextBox for example.\r\n\r\nThe idea of Ajax is to retrieve information from the server without needing to redraw the whole page. So why is it that the MS AJAX raise a complete page load event when you click on a basic AJAX item on the page? Isn\'t the whole point to simply \"get what I need and insert that new value in the page\" ?  Why does it need to go through the complete page load/page draw event sequence? That\'s not AJAX, that\'s an MS workaround to behave like AJAX in the browser, but the server still treats it like a whole new page request. Disappointing on some of the more complex pages.','2012-02-06','0000-00-00 00:00:00',1021,0,1),(5,NULL,NULL,'Recommendations Against CSRF (aka XSS  cross site scripting)','','<p><style type=\"text/css\"><!--.style1 {color: #FF0000}.code {border: 1px solid #666666;width: 500px;background-color: #efefef;padding: 10px;}.code .comment {color: #009933;}.instruction {color: #0000FF;}--></style><h1>Web Security</h1>\r\n<p><strong>(cross site request forgery) </strong> an attack whereby a user from an outside source sends a request to a web application that a user has already authenticated itself with, from a different website.p> <h3>Ways to prevent XSS</h3>  <p>  <strong>1. using $_POST not $_REQUEST on inputted fields</strong>  this will prevent malicious users from scripting elements that rely on a basic $_GET<br>  eg:<br><img src=\"http://www.example.com/process.php?amount=33&destination_card_number=1111222233334444\" /></p>  <p><strong>2. hashed token in session checked on each page</strong><br> eg:<br> (outgoing PHP Response)  send a unique ID token out to the users to-be-submitted form<br> </p><div class=\"code\">$token= <span class=\"instruction\">md5</span>(uniqid());<br>  <span class=\"instruction\">$_SESSION</span>[validationToken]= $token;<br>  <span class=\"instruction\">session_write_close</span>();<br>echo <input type=hidden name=token value=$token/>;</div> <p></p><p>(Incoming PHP Request)  check the token after it returns  make sure its still who we expect it to be<br></p><div class=\"code\">$token= <span class=\"instruction\">$_SESSION</span>[validationToken];<br> unset<span class=\"instruction\">($_SESSION</span>[validationToken]);<br> session_write_close();<br> if (<span class=\"instruction\">$_POST</span>[\'token\']==$token) {<br> <span class=\"comment\">// perform the requested action.</span><br> } else {<br> <span class=\"comment\">// log potential CSRF attack.</span><br> }</div> <p></p><p><strong>3. Putting a timeout on the hashed token</strong> (I didnt say token hash). <br></p><div class=\"code\"> $token_age = time() - <span class=\"instruction\">$_SESSION</span>[\'token_time\'];<br> if ($token_age <= 300) //300 seconds  an arbitrary time for this example<br> {<br> <span class=\"comment\">/* Less than five minutes has passed. */</span><br>}</div><p></p><p><strong>4. Enforcing session timeout and limit it to a minimum.</strong><br> How this can affect security:<br>a) The user leaves workstation and browser cache. Someone else can access. medium risk.</p><p>b) The more serious in your case, session hijacking. To hijack a session all one needs is the sessionid. Normally you\'d check if the session belongs to the user, but if session identifies the user you can\'t. Then all that is required to hijack a session, is to guess (easier if never expires) or catch with a network sniffer.</p><p><strong>5. Don\'t waste time with checking <span class=\"instruction\">$_SERVER</span>[\'HTTP_REFERER\'] </strong>since that only stops attacks from a browser <br> <span class=\"style1\">**everyones initial response to how should we secure the system?</span><br> <br> <strong>6. Put into htaccess file directives that disallow SQL injection keywords in the query string</strong> (delete/drop/insert etc)<br> <br> <strong>7. Fail open error handling</strong> (remove all \'ugly error\' messages). <br> Users should NEVER know what error happened, only that an error DID happen. This is where logging comes in for our debug audit on a proper log file. Showing the user that a file at /home/fvreb/public_html/includes/lang/en/eng-ca.php could not be found is not a wise solution for showing hackers the directory structure.<br> <br> <strong>8. Validation</strong>  make sure banned terms and invalid characters are not passed into the system.<br> <br> <strong>9. Use striptags() on user input</strong> before saving to remove NULL bytes, HTML and PHP tags stripped from a given string input.<br> <br> <strong>10. Using a captcha image on submitted forms.</strong> Ensures its not a script executing.<br> <br> <strong>11. HTML encode user-supplied data that is embedded into a returned page</strong> (ie a confirmation before submition). Again this would be preventing things like this from being printed to the page:<br><img src=\"http://www.example.com/process.php?amount=33&destination_card_number=1111222233334444\" /></p><p><strong>12. Post vs Get in Ajax calls.</strong> <br>GET should be used for what it is intended for: Getting data from the server (not sending), without changing state on the server. It should not be used to update resources on the server. POST or PUT are designed for that. Using HTTP the way it was intended to be used both helps avoid some (but far from all) XSS attacks, it also makes browsers behave a lot nicer when communicating with your site. The browser expects that GET requests can be safely repeated, without requiring confirmation from the user. That\'s what it does if you refresh the page, for example, or use the back/forward buttons. POST is expected to change state on the server, so the browser typically asks for confirmation before repeating a POST request.</p><h3>AJAX Issues</h3><p> Many issues can be found in AJAX calls since developers often pass queries as GET methods out of habit. Changing the queries to POST and then checking to ensure that they are only posted parameters on the receiving end also helps mitigate SQL and XSS injections.<br> Adding the random tokens on all queries will help against XSS  JQUERY has a built in method. Standard AJAX outside of frameworks will need to be manually adjusted.<br> <br> <strong>JQuery Tricks</strong><br> To use this token with jQuery, you need to make it available to javascript. You typically do this by adding it as a javascript variable.<br> var csrf_token = \'<%= token_value %>\';//random generated query drawn on serverside</p><p>Next, the trick is to bind to the globalajaxSendevent, and add the token to any POST request<br></p><div class=\"code\">$(\"body\").bind(\"ajaxSend\", function(elm, xhr, s){ <blockquote>if (s.type == \"POST\") { <blockquote>xhr.setRequestHeader(\'X-CSRF-Token\', csrf_token);</blockquote> }<br> </blockquote>});</div><br><p></p><h3>Ways to Track and Trace XSS Attempts</h3><p> 1. Maintain audit logs and create a banned IP list<br> 2. Key event should be logged, which typically include:</p><ul> <li>  all events relating to the authentication functionality, such as successful and failed login, and change of password</li> <li>  key transactions, such as credit card payments and funds transfers</li> <li>  access attempts that are blocked by the access control mechanisms</li> <li>  any requests containing known attack strings that indicate overtly malicious intentions</li> <li>  logging used by banks will track every single client request with complete forensic record that can be used to investigate further incidents and allow for fixes based on that information</li> <li>  log time of each event, IP address, session token, user\'s account (if authenticated)<br> </li>  <span class=\"style1\">**these logs need to be strongly protected against unauthorized re/write access. An effective approach is to store audit logs on an autonomous system that accepts only update messages from the main application. In some situations, logs may be flushed to a write once media (CD) to ensure integrity in the event of a successful attack</span><br><li>poorly protected audit logs can provide a gold mine of information to an attacker, disclosing a host of sensitive information such as session tokens and request parameters that may enable them to immediatley comprimise the entire application</li></ul>   <p>3. installation of alerting mechanisms that monitor traffic and anomalies such as:</p>  <ul>   <li>    large numbers of requests being received from a single IP address or user, indicating a scripted attack</li>   <li>    business anomalies, such as unusual number of funds transfers to/from a single account</li>   <li> requests containing known attack strings</li>   <li>    requests where data that is hidden from ordinary user has been modified (eg: token changed)</li>   <li>    off the shelf intrusion detection software on server<br>    <span class=\"style1\">Its important to note that reacting to apparent attacks is not a substitute for fixing vulnerabilities but even the most efforts to purge an application of security flaws may leave some exploitable defects remaining. Placing further obstacles in the way of an attacker is an effective defense-in-depth measure that reduces the likelihood that residual vulnerabilities will be found and exploited.</span> </li></ul>    <h3>Validation and Sanitization</h3>    <p><strong>Validation</strong>  preventing the process from continuing due to the presence of disallowed characters<br>     <strong>Sanitization</strong>  removal of disallowed characters without disrupting the flow of the process<br>  Things to check for:</p>    <ul>     <li>      control characters</li>     <li>      non alphanumeric (symbols, etc)</li>     <li>      excessive lengths</li>     <li>      spam</li>     <li>      binary data</li>     <li>      alternate encoded data (ascii, unicode, utf-8, hex, octal)</li>     <li>      sql injection </li>     <li>      code injection                </li>    </ul><br>      <strong>A basic Sanitization routine could recursively run on an inputted value until it detects no further changes.</strong><br>      1. strip any <script> expressions that appear<br>      2. truncate input <br>      3. remove quotation marks<br>      4. url decode input<br>      5. if any items deleted/changed then return to step 1. may want to flag on certain rules (eg rule 1)                <h3>Brief Recap of XSS Security Holes</h3>    <ol> <li>Lack of input validation on user input</li> <li>  Lack of sanitization on received input</li> <li>  Lack of sufficient logging mechanism</li> <li>  Sufficient logging mechanism with unsecure storage (a different server)</li> <li>  Fail-open error handling</li> <li>  Not closing the database connection properly</li> <li>  Lack of confirming who a user is (hashed token)</li> <li>  Htaccess configuration:<br>  RewriteCond %{QUERY_STRING} [^a-z](sql|declare|char|union|set|cast|convert|delete|drop|exec|insert|meta|script|select|truncate|update)[^a-z] [NC]<br>  RewriteRule (.*) - [F]</li></ol><h2>Bootstrapping</h2><p> Bootstrapping means that every server request are funneled through a single (or a few) PHP file. This file will be the bootstrapper of our application. It will help instantiate objects that are needed by every page in general such as starting a session, connecting to a database, defining constants and default variables, etc.<br> Reference from: <a href=\"http://www.serversidemagazine.com/php/bootstrap-php-code/\" target=\"_blank\">http://www.serversidemagazine.com/php/bootstrap-php-code/ </a></p><h3>How to Create a Bootstrap File</h3> Generallyits a good practice to setup a bootstrap filefor every PHP website or web application. This way a developer could easily manage the behavior of his application in a centralized manner.<br> This file is generally the main entry point on each HTTP request, usually theindex.phpfile in the document root.<br> It is important to mention that this file usually doesnt contain any HTML markup, just pure PHP that will load the template files if necessary or a front controller as how most of the MVC frameworks implement it.<br> <h3>What to Bootstrap</h3> <p>It is a good plan to create a mockup of functionalities that we wish to assign and implement in our file.<br> Usually a bootstrap file contains the necessary source code, libraries and logic to start the entire application. From showing figuring out what page to show, how to communicate with the database, etc.<br>Generally the file should contain the following initializations:</p> <ul>  <li>   Configuration</li>  <li>   Session, cookies</li>  <li>   Caching</li>  <li>   Database</li>  <li>   Directory and file paths</li>  <li>   Global variables and constants</li>  <li>   Web application status</li>  <li>   Web page routing</li>  <li>   Feeds</li>  <li>   XML/RPC</li>  <li>   Filtering of submitted values *</li>  <li>   Validation *</li>  <li>   Security checks *   <p></p>                            </li> </ul> <p>Setting Up<br> A bootstrap file usually starts by including those libraries that are necessary for the file itself to function correctly. This involves setting general directory and file paths, loading configuration files, etc.<br> <br> </p><div class=\"code\"> <span class=\"comment\">//let\'s set up a root path constant</span><br> <span class=\"instruction\">define</span>(\'ROOT\',getcwd().DIRECTORY_SEPARATOR);<br> <br> <span class=\"comment\">//define the includes and config folders</span><br> <span class=\"instruction\">define</span>(\'INCLUDES\',ROOT.\'includes\'.DIRECTORY_SEPARATOR);<br> <span class=\"instruction\">define</span>(\'CONFIG\',ROOT.\'config\'.DIRECTORY_SEPARATOR);<br> <br> <span class=\"comment\">//load in the main configuration file</span><br> <span class=\"instruction\">include_once</span>(CONFIG.\'base.inc.php\');<br></div> <p><br>  After the basic configuration we can write additional logic into the file, e.g. loading the session object, database object, etc. Its worth mentioning that every item from the list above should be an object/class, if we are using OOP, that manages that particular feature.<br> Its also encouraged that even for a basic web project we set up different application statuses such asDevelopment,ProductionorTestingand to load different libraries, logic for every status. <br> E.g. In production is a good practice to disable error display:<br> </p> <div class=\"code\"> <span class=\"comment\">//define our status constant</span><br> <span class=\"instruction\">define</span>(\'STATUS\',\'production\');<br> <br> <span class=\"comment\">//check what status we have</span><br> <span class=\"instruction\">switch</span> (STATUS) {<br> <blockquote><span class=\"instruction\">case</span> \'production\': { <blockquote><span class=\"instruction\">ini_set</span>(\'display_errors\',\'Off\');<br>  <span class=\"comment\">//and other specific includes, commands, etc.</span></blockquote> } </blockquote>}<br></div><p>Lastly we would set the page routing logic. A class that parses the HTTP request and renders the page by loading the necessary template files if any.<br>  After we set up the bootstrap file, we have to direct all request to this file. This is usually achieved with the help of a few rewrite rules in a.htaccessfile in our root directory (or that directory where theindex.phpfile is stored).<br></p><div class=\"code\"><IfModule mod_rewrite.c><br> RewriteEngine On<br> <br> RewriteCond %{REQUEST_FILENAME} !-f<br> RewriteCond %{REQUEST_FILENAME} !-d<br> <br> # Rewrite all other URLs to index.php/URL<br> RewriteRule ^(.*)$ index.php/$1 [PT,L]<br> <br></IfModule><br><IfModule !mod_rewrite.c><br> ErrorDocument 404 /index.php<br></IfModule></div><p></p>  <p></p><h3>PreProcessing</h3> <p> Preprocessing is telling the system to process other directives before handling the requested page.<br> This would be a good solution for systems that cannot utilize bootstrap methods as provided by organized frameworks<br> AUTO_PREPEND_FILE</p><ul> <li>  Found in php.ini file on server</li> <li>   Treats it like in include file function.</li> <li>   To disable it, leave it blank</li> <li>   Global across all files being parsed by server</li> <li>   Php.ini </li> <li>   auto_prepend_file = /server/path/filename.php</li> <li>   .htaccess</li> <li>   php_value auto_prepend_file /server/path/filename.php</li></ul><p><strong>Recommended reading:</strong><br> <a href=\"https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet\" target=\"_blank\">https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet </a><br> OWASP is the Open Source Web Application Security Project<br> <a href=\"http://ha.ckers.org/xss.html\" target=\"_blank\">http://ha.ckers.org/xss.html</a> cheat sheet of hacking methods to test for vulnerabilities<br> <a href=\"http://greatwebguy.com/programming/java/simple-cross-site-scripting-xss-servlet-filter/\" target=\"_blank\">http://greatwebguy.com/programming/java/simple-cross-site-scripting-xss-servlet-filter/</a><br> java based filter, but still a great recommendation  could be configured in an htaccess rule as well.<br> <a href=\"http://erlend.oftedal.no/blog/?blogid=118\" target=\"_blank\">http://erlend.oftedal.no/blog/?blogid=118</a> session based tokens in jquery<br></p><h2>Recommendations Against SQL Injection</h2><p>SQL injectionis an often used technique to attack databases through a website. This is done by including portions of SQL statements in a web form entry field in an attempt to get the website to pass a newly formed rogue SQL command to the database (e.g. dump the database contents to the attacker).<br> Often a good read of the apache logs will display multiple requests issued to test the vulnerable parameters such as userID, which involved injecting the string<br> 999\'+union+select+Table_name,Column_name+from+Information_schema.Columns+where+table_name=\'members\'+;# into the parameters value:</p><p> <strong>Limit table access on a connection object.</strong><br> Specify explicitly which tables a connection can access. Theres no reason for a user connection to have admin privileges across the database.<br></p><h3>Validation and Sanitization</h3> All items should be properly validated before submitting. Many systems sanitizes (removes unwanted characters), which is a great start, but look at it this way:<br> The system needs to make apple sauce. The first step is to remove anything not wanted in the sauce (the apple peel). So we write a sanitization method removePeel() and then pass it on to the makeSauce() routine. But what if we didnt have an apple in the first place? What if the user passed us an orange? We sanitized the object (removePeel()  the unwanted characters) but at the very end we added an orange to the mix.<br> <br>An example would be placing an email in the name field. We might remove all unwanted characters (@, .) but the the persons name is now:<br>davemeikleymailcom <br>which is basically the email address sanitized for a persons full name.<br> Sanitizing will remove unwanted characters but allow the system to continue forward not checking to see if there are any other security rules being broken, while validation will discontinue until all invalid inputs and characters have been removed.<br> <h3>Casting / Type Handling</h3> Strongly typed variables is a terrific yet simple trick in mitigating SQL Injections. By forcing a passed parameter to be cast as the proper data type it is meant to be, this can help to throw an error (stopping the execution) or discarding the crunchy bits (the SQL injection code) attached. This is an advantage other languages (such as Java or C) have over basic scripted languages such as PHP that do not enforce strong data types.<br> Eg:<br> <div class=\"code\"> $age =$_POST[age];<br> <span class=\"comment\">//where $age=17 from the request object</span><br> Vs.<br> $age =$_POST[age];<br> <span class=\"comment\">//where $age=17\'+union+select+Table_name,Column_name+<br> from+Information_schema.Columns+where+table_name=\'members\'+;#;</span></div> By casting $age as an integer, the system will either set $age to 17 and ignore the rest, or simply throw an error  depending on the language used.<br> Parameterized Queries / Stored Procedures <br> Parameterized Queries seriously mitigate the chances of malicious code getting into your SQL. PHP has a new class called PDO (Prepared Data Object) which offers a comfortable level of abstraction away from the database.<br> ($conn is a PDO object)<br> <div class=\"code\">$stmt = $conn->prepare(\"INSERT INTO tbl VALUES(:id, :name)\");<br> $stmt->bindValue(\':id\', $id);<br> $stmt->bindValue(\':name\', $name);<br>$stmt->execute();</div><p></p><p>Stored Procedures are similar to parameterized queries, except they cast their received arguments, forcing them to be clean. Write them once, theyre out of the way and cached in database server memory which actually helps speed up queries.</p><p><strong>SPRINTF()</strong><br> sprintf() can be used with conversion specifications to ensure that the dynamic argument is treated the way it\'s supposed to be treated (auto-casting).<br> $id = $_GET[\'id\'];<br>$query = sprintf(\"SELECT username FROM users WHERE id = \'%d\' \", $id);</p><p><strong>HTMLEntities()</strong><br> htmlentities() in conjunction with the optional secondquote_styleparameter, allows the use ofENT_QUOTES, which will convert both double and single quotes. This will work in the same sense as addslashes() and mysql_real_escape_string() in regards to quotation marks, however, instead of prepending a backslash, it will use the HTML entity of the quotation mark.<br> In addition to using ENT_QUOTES within htmlentities(), a third parameter can be set which forces the use of a character set within conversion. This will help stop unpredicted results from using multibyte characters in character sets such as BIG5 and GPK.<br> The following is an example of code which would help to prevent SQL injection in PHP.<br></p><div class=\"code\">$id = $_GET[\'id\'];<br> $id = htmlentities( $id, ENT_QUOTES, \'UTF-8\' );<br> <br> $query = \'SELECT username FROM users WHERE id = \"\' . $id . \'\"\';</div> <h3>HTACCESS Rules</h3><p> </p><div class=\"code\">RewriteCond %{QUERY_STRING} [^a-z](sql|declare|char|union|set|cast|convert|delete|<br>  drop|exec|insert|meta|script|select|truncate|update)[^a-z] [NC]<br>RewriteRule (.*) - [F]</div> Deny any information passed in a URI string (query based) that has database characters. The only downside is sometimes you want to pass the term action=delete on a clicked link  this would now be banned by the htaccess rules, so a modification would need to be performed on code to change delete to del or remove or castigate...<br></p>','2012-05-18','2014-04-10 06:39:49',889,0,1);
/*!40000 ALTER TABLE `Blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Customers`
--

DROP TABLE IF EXISTS `Customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `businessName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `States_id` int(11) DEFAULT NULL,
  `Countries_id` int(11) DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tollFree` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `domainName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dbUsername` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dbPassword` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dbName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dbHost` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Customers`
--

LOCK TABLES `Customers` WRITE;
/*!40000 ALTER TABLE `Customers` DISABLE KEYS */;
INSERT INTO `Customers` VALUES (1,'newrest1 test@#$%','address','city',1,1,'90210','604-722-9772','1-800-999-1111','fax','info@myco.com','2013-12-04 21:13:59','myco.com','project_user','R3dR0p#l1ckr1sh','1_newrest1_test','localhost'),(2,'my new business','address','city',1,1,'90210','604-722-9772','1-800-999-1111','fax','info@myco.com','2013-12-04 06:47:50',NULL,NULL,NULL,NULL,NULL),(3,'test db','q','q',0,0,'q','q','q','q','q','2013-12-04 07:15:45','q',NULL,NULL,NULL,NULL),(4,'test db','q','q',0,0,'q','q','q','q','q','2013-12-04 07:17:49','q',NULL,NULL,NULL,NULL),(5,'test db','q','q',0,0,'q','q','q','q','q','2013-12-04 07:19:52','q',NULL,NULL,NULL,NULL),(6,'\'rest test\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 07:20:39',NULL,NULL,NULL,NULL,NULL),(7,'\'rest test\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 07:22:56',NULL,NULL,NULL,NULL,NULL),(8,'\'rest test\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 07:23:33',NULL,NULL,NULL,NULL,NULL),(9,'\'rest test\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 07:24:39',NULL,NULL,NULL,NULL,NULL),(10,'\'rest test\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 07:25:25',NULL,NULL,NULL,NULL,NULL),(11,'\'rest test\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 07:26:16',NULL,NULL,NULL,NULL,NULL),(12,'\'rest test\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 07:26:56',NULL,NULL,NULL,NULL,NULL),(13,'\'rest test\'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 07:27:14',NULL,NULL,NULL,NULL,NULL),(14,'test db','q','q',0,0,'q','q','q','q','q','2013-12-04 07:27:35','q',NULL,NULL,NULL,NULL),(15,'qqwe','qwe','qwe',0,0,'qwe','qwe','qwe','qwe','qwe','2013-12-04 18:56:49','qwe',NULL,NULL,NULL,NULL),(16,'qqwe','qwe','qwe',0,0,'qwe','qwe','qwe','qwe','qwe','2013-12-04 19:06:42','qwe',NULL,NULL,NULL,NULL),(17,'qqwetest&this','qwe','qwe',0,0,'qwe','qwe','qwe','qwe','qwe','2013-12-04 20:43:19','qwe',NULL,NULL,NULL,NULL),(18,'qlqwetest&this','qwe','qwe',0,0,'qwe','qwe','qwe','qwe','qwe','2013-12-04 21:09:23','qwe',NULL,NULL,NULL,NULL),(19,'newrest1 test@#$%',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 21:21:26',NULL,NULL,NULL,NULL,NULL),(20,'newrest1 test@#$%',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 21:27:25',NULL,NULL,NULL,NULL,NULL),(21,'newrest1 test@#$%',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-12-04 21:28:19',NULL,NULL,NULL,NULL,NULL),(22,'qlqwetest&this','qwe','qwe',0,0,'qwe','qwe','qwe','qwe','qwe','2013-12-04 21:37:35','qwe','project_user','R3dR0p#l1ckr1sh','22_qlqwetestthis','localhost'),(23,'qlllqwetest&this','qwe','qwe',0,0,'qwe','qwe','qwe','qwe','qwe','2013-12-04 21:43:18','qwe','project_user','R3dR0p#l1ckr1sh','23_qlllqwetestthis','localhost');
/*!40000 ALTER TABLE `Customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DocumentCategories`
--

DROP TABLE IF EXISTS `DocumentCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DocumentCategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `lastModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DocumentCategories`
--

LOCK TABLES `DocumentCategories` WRITE;
/*!40000 ALTER TABLE `DocumentCategories` DISABLE KEYS */;
INSERT INTO `DocumentCategories` VALUES (1,'Residential Leasing',0,0,'2013-12-07 17:57:42',1),(2,'Residential Sales',0,0,'2013-12-07 17:57:50',1),(3,'Residential Rentals',0,0,'2013-12-07 17:58:00',1);
/*!40000 ALTER TABLE `DocumentCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LoginAccess`
--

DROP TABLE IF EXISTS `LoginAccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LoginAccess` (
  `userToken` varchar(40) NOT NULL,
  `decayTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ipAddress` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`userToken`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LoginAccess`
--

LOCK TABLES `LoginAccess` WRITE;
/*!40000 ALTER TABLE `LoginAccess` DISABLE KEYS */;
/*!40000 ALTER TABLE `LoginAccess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PageContents`
--

DROP TABLE IF EXISTS `PageContents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PageContents` (
  `pageName` varchar(50) NOT NULL,
  `content` text,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PageContents`
--

LOCK TABLES `PageContents` WRITE;
/*!40000 ALTER TABLE `PageContents` DISABLE KEYS */;
INSERT INTO `PageContents` VALUES ('Contact','coming soon...','title test','test description'),('About','<div class=\"about\">\n            <h1>about</h1>\n            <div>\n                <h2>Who is this guy anyways? </h2>\n                <p>Dave Meikle (Vancouver, BC and Phoenix, AZ) is the founder of Quantum Unit Solutions Software Design. Aside from running a contract development firm aimed at custom solutions for medium sized businesses (20-100 employees), Dave Meikle spent 3 years in Mainland China, where he oversaw the production and manufacturing of tier 1 networking equipment and provided quality assurance testing for North American buyers. </p>\n            </div>\n            <div>\n                <h2>But isn&apos;t he just another programmer? </h2>\n                <p>As the .big brain. behind all design and technology infrastructure for The Symons Internet Group, he was the Chief Technology Architect in the development and programming of the WebCommunityBuilder&copy; technology and the technology strategy for <br>\n                network-stable scaleable expansion </p>\n                <p>Prior to becoming the Chief Technology Officer of The Symons Internet Group, Mr. Meikle&apos;s previous record of achievements includes the successful creation and development of some of the first online communities ever created in the history of the Internet.</p>\n            </div>\n            <div>\n                <h2>sure, but does he have anything to his credits? </h2>\n                <p>Dave Meikle has also developed real-time tracking systems for marathon athletes, leading-edge wireless applications, gambling systems, games, security systems, and voice recognition command systems. His real-time tracking software which was first used in the   HARRISdirect Seattle Marathon 2002, was toted as being the first real-time tracking in North America.</p>\n                <p>Dave Meikle is fully-conversant in virtually all major programming languages.</p>\n                \n      <p>David has worked throughout the world including assignments in Hong Kong \n        (PRC), Wuhan (Hubei, China), Phoenix (AZ), Bethesda (MD), Washington DC, \n        Omaha (NE), and Vancouver (BC)</p>\n                <p>Mr. Meikle is a former &apos;Rocket Scientist&apos; with an undergraduate background in Propulsion Engineering on scholarship in Chicago (with honors) and a Bachelor&apos;s Degree in Computer Systems in Arizona. Mr. Meikle graduate with honors from high school in Apache Junction, Arizona. </p>\n            </div>\n        </div>\n','About',''),('Services','<div class=\"services\">\n            <h1>services</h1>\n            <div>\n                <h2>There is a wide variety of services available from Dave Meikle through his consulting firm <a href=\"http://www.quantumunit.com\" target=\"_blank\">Quantum Unit Solutions</a>. Please feel free to check the business site for more information.</h2>\n                \n      <p>Still here? Well let&apos;s see... urchins booted...tall buildings lept... \n        directions given...old ladies helped across the street.. public service \n        announcements for the safety of all children...oh, and the following list \n        below also:</p>\n            </div>\n            <div>\n                <div class=\"leftAlignedDiv\">\n      <h4>Languages Spoken</h4>     \n                \n      <ul>\n          <li>English - Yes, I&apos;m a native speaker...</li>\n          <li>Mandarin - started to learning it when I was in my early 20s... \n            then spent 3 years in Mainland China in the software industry. Is \n            it any good? ..hai xing...</li>\n      </ul> \n      </div>\n      \n    <div class=\"leftAlignedDiv\">\n      <h4>Languages Written</h4>\n                \n        <ul>\n          <li>Java</li>\n          <li>PHP</li>\n          <li>C#</li>\n          <li>Visual Basic</li>\n          <li>PLC (Programmable Logic Controllers)</li>\n          <li>Javascript / JQuery</li>\n        </ul>\n                \n      </div>\n      \n    <div class=\"leftAlignedDiv\">\n                <h4>Technologies</h4>\n                \n        <ul>\n          <li>XML-RPC</li>\n          <li>RMI</li>\n          <li>COM/DCOM</li>\n          <li>Network Programming</li>\n        </ul>\n                </div>\n            </div>\n            <div>\n                \n      <div class=\"cleared\"> <img src=\"/images/settings.jpg\" alt=\"\" />CHALLENGING \n        REQUIREMENTS ARE SOME OF THE BEST THINGS TO HAVE ON YOUR PLATE. \n        <p>Let&apos;s face it - you go to work every day, deal with the same people, \n          the same requirements, often at the same desk. Wouldn&apos;t it be great \n          if someone came in and said &quot;We&apos;ve got something the other guys \n          said just cannot be done..!&quot; *Ding!! *rings the bell..*<br>\n          That&apos;s the stuff that makes me keep my shoes shinier than normal. The \n          excitement of trying to do something that hasn&apos;t been done yet - ah, \n          yes.. the untouched snow of computer languages... *dreamy sigh*. But \n          hey, I love code. Any kind. If you need it designed, written, or simply \n          debugged... I&apos;m all in.</p>   \n                </div>      \n                <div>\n                    <img src=\"/images/globe2.jpg\" alt=\"\" />      \n                    \n        <h3>Do you have any outstanding situations that are worth mentioning?</h3>      \n                    \n        <p> Well, this one time I was in a little pub in Guangzhou, China... *whoops..*<br>\n          So we were trying to get Ozzie on stage and he says that he&apos;s not going \n          out there until someone brings him 1,000 brown M &amp; Ms..<br>\n          Actually I designed a web app back in 2000 called WapIt2Me - you could \n          create a private account upon registering, then using the browser, upload \n          a variety of documents into your account - an online briefcase. Then, \n          let&apos;s say your in New York for the weekend and someone says &quot;Hey, \n          I really liked what you had to say at that seminar.. could you send \n          me a copy of your executive summary?&quot; uh-oh. I didn&apos;t bring it \n          with me...oh, wait! *pulls out mobile phone with monochromatic screen \n          and WAP functionality...logs into website...selects the executive summary \n          pdf document... sends it to the recipient&apos;s email account as an attachment \n          - all from a mobile phone predating smart phones...*</p>\n                </div>  \n                <div> <img src=\"/images/stats.jpg\" alt=\"\" /> \n        <h3>Looking for more Proof? Just browse through all the articles and free \n          downloads to find what you&apos;re looking for.</h3>   \n                    \n        <p>But if you don.t find any article information nor free samples you \n          can use, you can try my<a href=\"http://www.quantumunit.com/\"> Web Design</a> \n          service and tell me what your requirements are. Maybe you.re looking \n          for something different, something special. And I love the challenge \n          of doing something different and something special.</p>\n                </div>\n            </div>\n        </div>','Software Services',''),('Products','<div class=\"products\">\n            <h1>product</h1>\n            <div>\n                <h2>who doesn&apos;t love free software?</h2>\n                <p>Below is a list of free software downloads and tools. These are all my own design and I offer them free for use provided you keep any credits to my work intact. You are also free to make edits as you see fit to work for your situation.</p>          \n            </div>\n            <div>\n                <div>\n                    \n        <h3>Cool tools to help your development business</h3>       \n                    <img src=\"/images/discuss2.jpg\" alt=\"\" />        \n                    \n        <p>validation library - php</p>\n        <p>role authentication menu - php</p>\n        <p>XML-RPC architecture - php/java</p>\n        <p>Token Authentication for mitigating Cross Site Request Forgery - php</p>\n                </div>      \n                <div>\n                    \n        <h3>Easy to use frameworks for your website development</h3>            \n                    <img src=\"/images/flags2.jpg\" alt=\"\" />  \n                    \n        <p><a href=\"https://github.com/dmeikle/gossamerCMS\" target=\"_blank\">Gossamer RESTful API CMS Client</p>\n        <p><a href=\"https://github.com/dmeikle/GossamerCMS-RESTful-database-API\" target=\"_blank\">Gossamer RESTful API CMS Database Server</p>\n        <p>&nbsp;</p>\n                </div>  \n                <div>\n                    \n        <h3>simple plugins for your website </h3>   \n                    <img src=\"/images/graph3.jpg\" alt=\"\" />      \n                    \n        <p>Blog Software - php </p>\n        <p>Shopping Cart - php $100</p> \n                </div>\n            </div>\n            \n    <p>This website is basically a simple HTML marketing tool to showcase a bit \n      of the stuff that I&apos;ve written in the past. Unfortunately, most of the development \n      has been &apos;work for hire&apos; where the employer owns the software that I write \n      for them. Many cases have been under Non-disclosure agreements so I&apos;ve been \n      unable to showcase a majority my work over the last 8 years. I write in \n      C#, Java and PHP. I love all the languages I write with although admittedly \n      I think Java is the forerunner in defining how good object oriented software \n      and design patterns should be implemented.</p>\n        </div>\n','Software Downloads','');
/*!40000 ALTER TABLE `PageContents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServerAuthenticationTokens`
--

DROP TABLE IF EXISTS `ServerAuthenticationTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServerAuthenticationTokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Customers_id` int(11) DEFAULT NULL,
  `serverName` varchar(40) DEFAULT NULL,
  `ipAddress` varchar(15) DEFAULT NULL,
  `token` varchar(40) DEFAULT NULL,
  `expirationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastRequestTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServerAuthenticationTokens`
--

LOCK TABLES `ServerAuthenticationTokens` WRITE;
/*!40000 ALTER TABLE `ServerAuthenticationTokens` DISABLE KEYS */;
INSERT INTO `ServerAuthenticationTokens` VALUES (1,0,'gossamercms','72.4.146.128','bQd18aba1df45jk7858l3g@lae88a57fa30','2014-04-13 03:52:50','2013-11-15 05:00:00'),(2,0,'localhost','99.199.92.67','bQd18aba1df45jk7858c8ae88a57fa30','2014-04-13 03:51:30','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `ServerAuthenticationTokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SystemAnnouncements`
--

DROP TABLE IF EXISTS `SystemAnnouncements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SystemAnnouncements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Categories_id` int(11) DEFAULT NULL,
  `Staff_id` int(11) DEFAULT NULL,
  `dateEntered` date DEFAULT NULL,
  `expirationDate` date DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SystemAnnouncements`
--

LOCK TABLES `SystemAnnouncements` WRITE;
/*!40000 ALTER TABLE `SystemAnnouncements` DISABLE KEYS */;
/*!40000 ALTER TABLE `SystemAnnouncements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SystemAnnouncementsI18n`
--

DROP TABLE IF EXISTS `SystemAnnouncementsI18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SystemAnnouncementsI18n` (
  `SystemAnnouncements_id` int(11) NOT NULL DEFAULT '0',
  `locale` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastModified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`SystemAnnouncements_id`,`locale`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SystemAnnouncementsI18n`
--

LOCK TABLES `SystemAnnouncementsI18n` WRITE;
/*!40000 ALTER TABLE `SystemAnnouncementsI18n` DISABLE KEYS */;
/*!40000 ALTER TABLE `SystemAnnouncementsI18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Customers_id` int(11) DEFAULT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `roles` varchar(100) DEFAULT NULL,
  `passwordHistory` varchar(500) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `lastSuccessful` datetime DEFAULT NULL,
  `lastUnsuccessful` datetime DEFAULT NULL,
  `unsuccessfulLoginCount` int(11) DEFAULT NULL,
  `unsuccessfulLoginMax` int(11) DEFAULT NULL,
  `passwordSetTime` datetime DEFAULT NULL,
  `passwordResetTime` datetime DEFAULT NULL,
  `forceReset` tinyint(1) DEFAULT NULL,
  `isLocked` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,22,'Dave','Meikle','davemeikle@ymail.com','9e605283d92bc1005927f148fb461ce4','admin, user','[\"cb05a65efc2bf0b9c5f478e689eb0f31\",\"d6f89cad81f5bdc7ce98cd75ad73e880\",\"e25bb419979b97e9afd800509cd58aa6\",\"875ba3a554adf51147366dbb134b0ad4\",\"f1d4ebaf45f90e6e4a4cbcb0ea1ec337\",\"3872ae0279e2c9fb3ebf2d612b5e0cc0\",\"9e605283d92bc1005927f148fb461ce4\"]','6047229772',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,22,'Carl','Nyvlt','carlnyvlt@gmail.com','b8c24146302da24c6f72ce73abfb7c61',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-16 16:07:28
