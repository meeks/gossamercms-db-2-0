GossamerCMS-RESTful-database-API
================================

This is version 2 - coming from a Java background it was driving me nuts putting everything into segregated folders. 
I've decided to follow my Java heart and make each component hold its own associated files - thank goodness for
the namespacing I added!

************************************* 

Part 2 of the GossamerCMS project. This is the central database that portal CMS sites can CRUD their data.

First off, I want to say that 'namespacing' is coming soon and I apologize for having to remove it.
Not sure what happened but was having a bear of a time getting symfony2 class loader to work when it
came down to testing all the work. I had to remove all of it in the meantime (shudder) and have it
working from the get go with the gossamerCMS client side, so will look at re-implementing it into the
DB side.

For starters, the DB connection info (for now) is located in the /classes/database/DBConnection.php file.
I do not recommend this - it was a quick down and dirty as I was writing this restful API from the top
of my head over the period of 1 full weekend and a week of evenings. I will move this in a later version.

Responding to REST calls to/from the database:
step 1:
create a table in your database. eg:
<pre>
create table Members (
	id int not null auto_increment,
	email varchar(50) not null,
	firstname varchar(20) not null,
	lastname varchar(20) not null,
	telephone varchar(20) not null
);
</pre>

Step 2A (single language support):
create a class file called Member in the '/classes/entities' folder (eg: classes/entities/Member.php)
The system will automatically map the database columns and queries accordingly. For a standard,
single language table simply extend the entity class from the AbstractEntity class. eg:

<pre>
class Member extends AbstractEntity implements SQLInterface
{

    public function __construct(){
        parent::__construct();
    }
}
</pre>

Step 2B (multi-language support)
for a multi-language support table structure, create a secondary table that maps locales and text fields
based on the parent table's id. The secondary table MUST have the same name as the parent, with 'I18n' appended
to its table name:
<pre>
create table Documents (
	id int not null auto_increment,
	Categories_id int not null,
);
create table DocumentsI18n (
	Documents_id int not null,
	locale char(5) not null default 'en_US',
	title varchar(50),
	content text,
	UNIQUE KEY `documents_locales` (`Documents_id`,`locale`)
);
</pre>
The table must have the unique key of parent table row ID combined with locale. This would indicate the following:
Documents_id	locale	title 						content
1				en_US	sample title 				this is sample content
1				fr_CA	titre de l'échantillon		c'est le contenu échantillon
2				en_US	another title 				this is another sample title

this means document 1 has 2 locales, each with their own content, determined by the 2nd key locale
this also means document 2 has 1 locale, with its own content differentiated from the other document by its Documents_id value

Now the entity class simply needs to extend from the AbstractI18nEntity class. The system will handle the multi-locale support
automatically behind the scenes with no extra coding required.
<pre>
class Document extends AbstractI18nEntity implements SQLInterface
{

    public function __construct(){
        parent::__construct();
    }
}

</pre>


**notes - not really satisfied with some of the naming conventions I used for filters/observers. Feels a bit clunky at the moment, 
and it showed me I could have used another layer of separation between a 'snap-in' class and how I implemented the Chain-of-Responsibility
design pattern and the Observer/Observables. That'll teach me to spend a bit more time drawing the blueprint rather than simply
sitting in the cafeteria of a college on a Saturday afternoon while my daughter sits in a kid's language class for 2 hours...
