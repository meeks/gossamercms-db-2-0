<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

//these are included in order of operation - do not change!
include_once('includes/configuration.php');
include_once('vendor/autoload.php');
include_once('includes/init.php');
include_once('includes/bootstrap.php');

use core\system\Kernel;

$kernel = new Kernel($container, $logger);
$kernel->run();

unset($kernel);
